import os
import sqlite3 as sql


class Database:

    def __init__(self):

        self.dbfile = "data" + os.sep + "database" + os.sep + "snippetdb.sqlite"

        if not os.path.exists("data"):
            os.mkdir("data")
        elif not os.path.exists("data" + os.sep + "database"):
            os.mkdir("data" + os.sep + "database")

        if not os.path.exists(self.dbfile):
            self.con = sql.connect(self.dbfile)
            self.create_database()
        else:
            self.con = sql.connect(self.dbfile)

        self.con.row_factory = sql.Row

    def create_database(self):

        with self.con:

            cur = self.con.cursor()
            cur.execute("CREATE TABLE Categories(CatID INTEGER PRIMARY KEY AUTOINCREMENT, ParentID INT, ChildrenID TEXT, CatName TEXT)")
            cur.execute("CREATE TABLE Snippets(SnipId INTEGER PRIMARY KEY AUTOINCREMENT, CatID INT, Snippet TEXT)")

    def add_category(self, parentid, name):

        with self.con:

            cur = self.con.cursor()
            cur.execute("INSERT INTO Categories (ParentID, CatName) VALUES(?, ?)", (parentid, name))

    def add_category_child(self, parentid):

        with self.con:

            cur = self.con.cursor()

            childrenids = self.get_category_children_ids(parentid)

            if childrenids is None:
                childrenids = self.get_latest_category_seq()
            else:
                childrenids = childrenids + "," + str(self.get_latest_category_seq())

            cur.execute("UPDATE Categories SET ChildrenID=? WHERE CatID=?", (childrenids, parentid))

    def remove_category(self, catid):

        todelete = self.get_removal_list(catid)

        try:
            for child in self.get_category_children_ids(catid).split(","):
                todelete.append((child,))
        except:
            pass
        finally:
            todelete.append((catid,))

        with self.con:

            cur = self.con.cursor()
            cur.executemany("DELETE FROM Categories WHERE CatID=?", todelete)

    def get_categories(self):

        with self.con:

            cur = self.con.cursor()
            cur.execute("SELECT * FROM Categories")

            return cur.fetchall()

    def get_category_parent_id(self, catid):

        with self.con:

            cur = self.con.cursor()
            cur.execute("SELECT * FROM Categories WHERE CatID=?", (catid,))

            return cur.fetchone()["ParentID"]

    def get_category_children_ids(self, catid):

        with self.con:

            cur = self.con.cursor()
            cur.execute("SELECT * FROM Categories WHERE CatID=?", (catid,))

            return cur.fetchone()["ChildrenID"]

    def get_removal_list(self, catid):

        toremove = self.crawl_category_children(catid) + [catid, ]
        toremove = ((childid,) for childid in toremove[:])

        return toremove

    def get_latest_category_seq(self):

        with self.con:

            cur = self.con.cursor()
            cur.execute("SELECT * FROM sqlite_sequence WHERE name='Categories'")

            return cur.fetchone()["seq"]

    def crawl_category_children(self, parentid):

        childrenlist = list(self.get_category_children_ids(parentid).split(","))

        for childid in childrenlist:
            childids = self.get_category_children_ids(childid)
            if childids is not None:
                for childid in childids.split(","):
                    childrenlist.append(childid)

        return childrenlist

    def add_snippet(self, catid, snippet):

        with self.con:

            cur = self.con.cursor()
            cur.execute("INSERT INTO Snippets (CatID, Snippet) VALUES(?, ?)", (catid, snippet))

    def remove_snippet(self, snipid):

        pass

    def get_snippets(self, catid):

        with self.con:

            cur = self.con.cursor()
            cur.execute("SELECT * FROM Snippets WHERE CatID=?", (catid,))

            return cur.fetchall()

    def get_snippet(self, snipid):

        with self.con:

            cur = self.con.cursor()
            cur.execute("SELECT * FROM Snippets WHERE SnipID=?", (snipid,))

            return cur.fetchone()
