import sys
from PySide import QtCore
from PySide import QtGui

from . import mainwindow


class Snippet(QtGui.QMainWindow, mainwindow.Ui_MainWindow):

    def __init__(self, db):

        super().__init__()

        self.setupUi(self)
        self.make_connections()
        self.add_tree_actions()

        self.db = db

        self.create_tree()
        self.get_snippets()

    def make_connections(self):

        # Actions
        self.addmaincatAction.triggered.connect(self.add_category)
        self.addchildcatAction.triggered.connect(self.add_child_category)
        self.removecatAction.triggered.connect(self.remove_category)

    def add_tree_actions(self):

        self.CatTree.addAction(self.addmaincatAction)
        self.CatTree.addAction(self.addchildcatAction)
        self.CatTree.addAction(self.removecatAction)

        self.CatTree.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)

    def create_tree(self):

        # items must be sorted by id

        cats = self.db.get_categories()

        catdict = {}
        for cat in cats:
            if cat["ParentID"] == -1:
                catdict[cat["CatID"]] = QtGui.QTreeWidgetItem(self.CatTree)
                catdict[cat["CatID"]].setText(0, cat["CatName"])
                catdict[cat["CatID"]].setData(0, QtCore.Qt.UserRole, cat["CatID"])
            else:
                catdict[cat["CatID"]] = QtGui.QTreeWidgetItem(catdict[cat["ParentID"]])
                catdict[cat["CatID"]].setText(0, cat["CatName"])
                catdict[cat["CatID"]].setData(0, QtCore.Qt.UserRole, cat["CatID"])

    def add_category(self):

        name = self.show_prompt("Category name", "Enter a name for the category: ")

        if name is not None:
            self.db.add_category(-1, name)

            self.refresh_tree()

    def add_child_category(self):

        name = self.show_prompt("Category name", "Enter a name for the category: ")

        if name is not None:
            parentid = self.CatTree.currentItem().data(0, QtCore.Qt.UserRole)
            self.db.add_category(parentid, name)
            self.db.add_category_child(parentid)

            self.refresh_tree()

    def remove_category(self):

        catid = self.CatTree.currentItem().data(0, QtCore.Qt.UserRole)
        self.db.remove_category(catid)

        self.refresh_tree()

    def refresh_tree(self):

        self.CatTree.clear()
        self.create_tree()
        self.CatTree.expandAll()

#    def get_snippets(catId):
    def get_snippets(self):

        # search for snippets with same cat
        snips = ("filecwd", "gcd", "multisplit",
                 "similarity", "Word Slice List")

        for snip in snips:
            self.SnipList.insertItem(0, snip)

    def print_id(self):

        print(self.CatTree.currentItem().data(0, QtCore.Qt.UserRole))

    def show_prompt(self, title, question):

        text, ok = QtGui.QInputDialog.getText(self, title,
                                              question)

        if ok:
            return text
        else:
            return None


def exec_gui(db):

    app = QtGui.QApplication(sys.argv)
    snippet = Snippet(db)
    snippet.show()
    app.exec_()
