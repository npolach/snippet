#!/usr/bin/env python3

import os

os.chdir(os.path.split(os.path.realpath(__file__))[0])

from src import databasetools
from src.gui import mainapp


def main():

    db = databasetools.Database()

    mainapp.exec_gui(db)

if __name__ == "__main__":

    main()
